package jp.alhinc.sato_keisuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class CalculateSales {
	static final String stfBranchLst = "branch.lst";
	static final String stfBranchOut = "branch.out";
	static final String stfSalesEx = "rcd";

	static ArrayList<String> storeCodes = new ArrayList<String>();
	static HashMap<String, String> storeMap = new HashMap<>();
	static HashMap<String, Long> summaryMap = new HashMap<>();

	//フォーマット
	static final int stfCodeLen = 3;//支店コード桁数
	static final int stfLstItems = 2;//支店定義ファイル項目数
	static final int stfSalesFileNameLen = 8;//支店定義ファイル項目数

	public static void main(String[] args) {
		BufferedReader br = null;
		PrintWriter pw = null;
		String dir = args[0];

		try {
			File storeFile = new File(dir, stfBranchLst);

			notExistException(storeFile);

			FileReader fr = new FileReader(storeFile);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				branchException(line);
				String[] splitLine = line.split(",", 0);
				storeMap.put(splitLine[0], splitLine[1]);
				long ini = 0;
				summaryMap.put(splitLine[0], ini);
				storeCodes.add(splitLine[0]);
			}

			File rcdDir = new File(dir);
	        File files[] = rcdDir.listFiles();
	        ArrayList<Integer> rcdNameList = new ArrayList<>();
	        for(int i=0; i < files.length; i++){

	        	String fileEx = getSuffix(files[i].getName());
	        	String fileName = getPreffix(files[i].getName());
	        	//拡張子rcd、数字、8桁
	        	if ((fileEx.equals(stfSalesEx))
	        			&& (numCheck(fileName))
	        			&& (fileName.length() == stfSalesFileNameLen)
	        			&& (Integer.parseInt(fileName)) > 0) {
	        		rcdNameList.add(Integer.parseInt(fileName));

	        		fr = new FileReader(files[i]);
	    			br = new BufferedReader(fr);

	    			String code = "";
	    			long summary = 0;
	    			int lineCount = 0;

	    			while ((code = br.readLine()) != null) {
	    				storeCodeException (code, storeCodes, files[i].getName());
	    				summary = Long.parseLong(br.readLine()) + summaryMap.get(code);
	    				summaryException(summary);
	    				summaryMap.put(code, summary);

	    				lineCount++;
	    			}

	    			//エラー判定
	    			rcdFormatException(lineCount, files[i].getName());
	        	}
	        }
	        rcdSerialException (rcdNameList);

	        if (new File(dir + "\\" + stfBranchOut).exists()) {
	        	System.out.println("既にファイルが存在します。バックアップを保存し新規作成します。");

	        	if (!new File(dir + "\\bk").exists()) {
	        		new File(dir + "\\bk").mkdir();
	        	}
	        	LocalDateTime date = LocalDateTime.now();
	        	DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

	        	new File(dir + "\\" + stfBranchOut).renameTo(new File(dir + "\\bk\\" + dateFormat.format(date) + stfBranchOut));
	        	new File(dir + "\\" + stfBranchOut).createNewFile();

	        } else {
	        	new File(dir + "\\" + stfBranchOut).createNewFile();

	        }


	        pw = new PrintWriter(new BufferedWriter(new FileWriter(dir + "\\" + stfBranchOut)));
	        for (int i = 0; i < storeCodes.size(); i++) {
	        	pw.print(storeCodes.get(i) + "," + storeMap.get(storeCodes.get(i)) + "," + summaryMap.get(storeCodes.get(i)));

	        	if (i < storeCodes.size() - 1) {
	        		pw.print("\r\n");
	        	}
	        }

		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("予期せぬエラーが発生しました");

		} finally {
			if (br != null) {
				try {
					br.close();
					pw.close();
				} catch (IOException ex) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}

	/**
	 * 支店定義ファイルの有無チェック
	 * @param file 検索ファイル
	 */
	public static void notExistException(File file) {
		if (file.exists()) {
		} else {
			System.out.println("支店定義ファイルが存在しません");
			System.exit(0);
		}
	}

	/**
	 * 支店定義ファイルのフォーマットを確認
	 * @param str １行単位の読取データ
	 */
	public static void branchException(String str) {

		boolean result = false;
		String[] splitLine = str.split(",", 0);
		if ((splitLine[0].length() == stfCodeLen) && (splitLine.length == stfLstItems)) {
			//支店コード 3桁
			//1行の項目数 2

			result = numCheck(splitLine[0]);
		}
		if (result == true) {

		} else {
			System.out.println("支店定義ファイルのフォーマットが不正です");
			System.exit(0);
		}
	}

	/**
	 * 合計金額の桁数チェック
	 * @param str
	 */
	public static void summaryException(long summary) {
		if (String.valueOf(summary).length() > 10) {
			System.out.println("合計金額が10桁を超えました");
			System.exit(0);
		}
	}

	public static void rcdFormatException(int lineSize, String fileName) {
		if (lineSize > 1) {
			System.out.println("<" + fileName + ">のフォーマットが不正です");
			System.exit(0);
		}
	}

	public static void storeCodeException (String code, ArrayList<String> storeCodes, String fileName) {
		if (!storeCodes.contains(code)) {
			System.out.println("<" + fileName + ">の支店コードが不正です");
			System.exit(0);
		}
	}

	public static void rcdSerialException (ArrayList<Integer> rcdNameList) {
		Collections.sort(rcdNameList);
		for (int i = 0; i < rcdNameList.size(); i++) {
			if (rcdNameList.get(i) != (i + 1)) {
				System.out.println("売上ファイル名が連番になっていません");
				System.exit(0);
			}
		}
	}

	/**
	 * 数字かどうか判定
	 * @param str 判定対象の文字列
	 * @return true = 数字
	 */
	public static boolean numCheck(String str) {
		boolean result = false;
		try {
	        Integer.parseInt(str);
	        result = true;
	    } catch (NumberFormatException e) {
	    }
		return result;
	}

	/**
	 * 拡張子抜きのファイル名にする
	 * @param fileName 拡張子付きファイル名
	 * @return 拡張子なしのファイル名
	 */
	public static String getPreffix(String fileName) {
	    if (fileName == null)
	        return null;
	    int point = fileName.lastIndexOf(".");
	    if (point != -1) {
	        return fileName.substring(0, point);
	    }
	    return fileName;
	}

	/**
	 * 拡張子を取得する
	 * @param fileName 拡張子付きファイル名
	 * @return 拡張子
	 */
	public static String getSuffix(String fileName) {
	    if (fileName == null)
	        return null;

	    int point = fileName.lastIndexOf(".");
	    if (point != -1) {
	        return fileName.substring(point + 1);
	    }
	    return fileName;
	}
}
